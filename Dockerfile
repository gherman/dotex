FROM alpine

COPY texlive-profile.txt /tmp/
RUN apk add --no-cache libgcc perl python3 wget xz && \
    mkdir /tmp/install-tl && \
    wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
    tar -xzf install-tl-unx.tar.gz -C /tmp/install-tl --strip-components=1 && \
    rm install-tl-unx.tar.gz && \
    /tmp/install-tl/install-tl --force-platform=x86_64-linuxmusl --profile=/tmp/texlive-profile.txt && \
    rm -rf /tmp/install-tl
ENV PATH=/usr/local/texlive/bin/x86_64-linuxmusl:$PATH
COPY dotex /usr/local/bin/
RUN dotex --update
