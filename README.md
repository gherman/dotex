# dotex

**dotex** is a small python script similar in spirit to [texliveonfly](https://ctan.org/pkg/texliveonfly):
it compiles TeX files, auotmatically detecting and installing required packages.
It is not (yet?) part of CTAN.

The aim of the script is to be used for dockerized builds -
it allows the build image to be quite lightweight
(this repository also hosts an appropriate image, about 115 MB in size).

**dotex** uses three strategies to guess what packages are reuired:

- it looks at the command you want to execute and finds known binaries there
- it looks into the `.tex` files mentioned in this command,
  analysing its `\usepackage` and `\bibliographystyle` commands
- it runs the command and (if it fails) analyzes its output

To speed up the searching of packages containing the required files,
it keeps a local compressed cache of package contents.
The cache is only updated if explicitly requested.

# requirements

To use **dotex**, you need a TeX Live distribution with the **tlmgr** package manager.
Note that no particular packages need to be preinstalled:
the "infraonly" installation scheme is good enough!

As **dotex** is written in Python 3, you will also need that.

# usage

Imagine you would like to compile a .tex source file with a command like

`latexmk -pdf -interaction=nonstopmode document.tex`

In order to automatically detect and install the packages required by the above command,
simply prefix it by **dotex**, i.e.:


`dotex latexmk -pdf -interaction=nonstopmode document.tex`

Now watch the magic - and if it does not work, report an issue here!

Please note that packages are installed by simply running `tlmgr install` -
this means using this script usually needs administrative privileges.
This is usually not a problem in a docker container.

As mentioned above, **dotex** keeps a local cache of package contents.
The cache is populated if it does not exist, or when a `--update` option is used
(this has to come before the compilation command).
The location of the cache can be adjusted by specifying `--database <path>`.

# docker

In the registry connected to this repository,
we provide a docker image (based on Alpine Linux) able to run **dotex**.
The image contains an initial package cache, populated during build.
In order to compile TeX to PDF in a CI setup, specify:

- the image `registry.gitlab.com/gherman/dotex`
- the command, `dotex ...`, as above

If you compile multiple TeX sources,
it makes sense to do process of them in a single job,
as the packages installed for one of them will be available for subsequent compilations.
